#!/usr/bin/env bash

cp "$(go env GOROOT)"/misc/wasm/wasm_exec.js public/js/wasm_exec.js  || exit
cd wasm-src || exit
GOOS=js GOARCH=wasm go build -o ../public/thumbnailer.wasm .