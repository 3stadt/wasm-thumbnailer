#!/usr/bin/env bash

# TINYGO has a memory leak, so only use for demonstration purposes!

cp "$(tinygo env TINYGOROOT)"/targets/wasm_exec.js public/js/wasm_exec.js  || exit
cd wasm-src || exit
tinygo build -o ../public/thumbnailer.wasm -target wasm .