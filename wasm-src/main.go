package main

import (
	"bytes"
	"encoding/base64"
	"fmt"
	"image"
	_ "image/gif"
	"image/jpeg"
	_ "image/png"
	"log"
	"syscall/js"
)

func main() {
	c := make(chan bool)
	fmt.Println("wasm demo successfully loaded!")
	js.Global().Set("thumbnailer", js.FuncOf(thumbnailer))
	<-c
}

func thumbnailer(_ js.Value, args []js.Value) interface{} {
	b64str := args[0].String()
	file, err := base64.StdEncoding.DecodeString(b64str)
	if err != nil {
		return err.Error()
	}

	fileBytes := bytes.NewReader(file)

	srcImage, _, err := image.Decode(fileBytes)
	if err != nil {
		return err.Error()
	}

	fileBytes = bytes.NewReader(file)
	ic, _, err := image.DecodeConfig(fileBytes)
	if err != nil {
		return err.Error()
	}

	subImage := srcImage.(interface {
		SubImage(r image.Rectangle) image.Image
	}).SubImage(image.Rect(int(float64(ic.Width)*0.5), int(float64(ic.Height)*0.5), int(float64(ic.Width)*0.2), int(float64(ic.Height)*0.2)))

	out := bytes.Buffer{}

	err = jpeg.Encode(&out, subImage, &jpeg.Options{Quality: 90})
	if err != nil {
		log.Fatal(err)
	}
	return base64.StdEncoding.EncodeToString(out.Bytes())
}
